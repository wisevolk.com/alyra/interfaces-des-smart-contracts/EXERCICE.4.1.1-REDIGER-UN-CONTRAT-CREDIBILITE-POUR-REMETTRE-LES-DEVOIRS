pragma solidity >=0.5.7 <0.7.0;

import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";

contract Credibilite {

    using SafeMath for uint256;

    mapping (address => uint256) public cred;
    bytes32[] private devoirs;

    function produireHash(string memory _url) public pure returns (bytes32){
        return keccak256(abi.encodePacked(_url));
    }

    function transfer(address _destinataire, uint256 _valeur) public {
        require(cred[msg.sender] - _valeur > 1);
        cred[_destinataire] += _valeur;
        cred[msg.sender] -= _valeur;
    }

    function remettre(bytes32 _dev) public returns (uint) {
        devoirs.push(_dev);
        return devoirs.length;
    }

}
